<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180605083513 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE left_handed_weapon (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE environnement (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, background VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE item_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, pseudo VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE suffixe_map (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE cell (id INT AUTO_INCREMENT NOT NULL, map_id INT NOT NULL, x_coord INT DEFAULT NULL, y_coord INT NOT NULL, is_discovered TINYINT(1) NOT NULL, INDEX IDX_CB8787E253C55F64 (map_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE map (id INT AUTO_INCREMENT NOT NULL, hero_id INT NOT NULL, name VARCHAR(255) NOT NULL, background VARCHAR(255) NOT NULL, is_done TINYINT(1) NOT NULL, INDEX IDX_93ADAABB45B0BCD (hero_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE right_handed_weapon (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE suffixe_item (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hero_items (id INT AUTO_INCREMENT NOT NULL, hero_id INT NOT NULL, item_id INT NOT NULL, id_decoder INT NOT NULL, INDEX IDX_7E21E68A45B0BCD (hero_id), INDEX IDX_7E21E68A126F525E (item_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE hero (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, pseudo VARCHAR(255) NOT NULL, hero_class VARCHAR(255) NOT NULL, INDEX IDX_51CE6E86A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE items (id INT AUTO_INCREMENT NOT NULL, id_decoder INT NOT NULL, complete_item VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, suffixe VARCHAR(255) NOT NULL, rarity_name VARCHAR(255) NOT NULL, rarity_color VARCHAR(255) NOT NULL, str INT NOT NULL, sta INT NOT NULL, agi INT NOT NULL, wis INT NOT NULL, intel INT NOT NULL, luc INT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE cell ADD CONSTRAINT FK_CB8787E253C55F64 FOREIGN KEY (map_id) REFERENCES map (id)');
        $this->addSql('ALTER TABLE map ADD CONSTRAINT FK_93ADAABB45B0BCD FOREIGN KEY (hero_id) REFERENCES hero (id)');
        $this->addSql('ALTER TABLE hero_items ADD CONSTRAINT FK_7E21E68A45B0BCD FOREIGN KEY (hero_id) REFERENCES hero (id)');
        $this->addSql('ALTER TABLE hero_items ADD CONSTRAINT FK_7E21E68A126F525E FOREIGN KEY (item_id) REFERENCES items (id)');
        $this->addSql('ALTER TABLE hero ADD CONSTRAINT FK_51CE6E86A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE hero DROP FOREIGN KEY FK_51CE6E86A76ED395');
        $this->addSql('ALTER TABLE cell DROP FOREIGN KEY FK_CB8787E253C55F64');
        $this->addSql('ALTER TABLE map DROP FOREIGN KEY FK_93ADAABB45B0BCD');
        $this->addSql('ALTER TABLE hero_items DROP FOREIGN KEY FK_7E21E68A45B0BCD');
        $this->addSql('ALTER TABLE hero_items DROP FOREIGN KEY FK_7E21E68A126F525E');
        $this->addSql('DROP TABLE left_handed_weapon');
        $this->addSql('DROP TABLE environnement');
        $this->addSql('DROP TABLE item_type');
        $this->addSql('DROP TABLE user');
        $this->addSql('DROP TABLE suffixe_map');
        $this->addSql('DROP TABLE cell');
        $this->addSql('DROP TABLE map');
        $this->addSql('DROP TABLE right_handed_weapon');
        $this->addSql('DROP TABLE suffixe_item');
        $this->addSql('DROP TABLE hero_items');
        $this->addSql('DROP TABLE hero');
        $this->addSql('DROP TABLE items');
    }
}
