<?php

namespace App\Repository;

use App\Entity\SuffixeItem;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SuffixeItem|null find($id, $lockMode = null, $lockVersion = null)
 * @method SuffixeItem|null findOneBy(array $criteria, array $orderBy = null)
 * @method SuffixeItem[]    findAll()
 * @method SuffixeItem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SuffixeItemRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SuffixeItem::class);
    }

//    /**
//     * @return SuffixeItem[] Returns an array of SuffixeItem objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SuffixeItem
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
