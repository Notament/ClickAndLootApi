<?php

namespace App\Repository;

use App\Entity\SuffixeMap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method SuffixeMap|null find($id, $lockMode = null, $lockVersion = null)
 * @method SuffixeMap|null findOneBy(array $criteria, array $orderBy = null)
 * @method SuffixeMap[]    findAll()
 * @method SuffixeMap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SuffixeMapRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, SuffixeMap::class);
    }

//    /**
//     * @return SuffixeMap[] Returns an array of SuffixeMap objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SuffixeMap
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
