<?php

namespace App\Repository;

use App\Entity\LeftHandedWeapon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method LeftHandedWeapon|null find($id, $lockMode = null, $lockVersion = null)
 * @method LeftHandedWeapon|null findOneBy(array $criteria, array $orderBy = null)
 * @method LeftHandedWeapon[]    findAll()
 * @method LeftHandedWeapon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class LeftHandedWeaponRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, LeftHandedWeapon::class);
    }

//    /**
//     * @return LeftHandedWeapon[] Returns an array of LeftHandedWeapon objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('l.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?LeftHandedWeapon
    {
        return $this->createQueryBuilder('l')
            ->andWhere('l.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
