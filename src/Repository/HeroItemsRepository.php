<?php

namespace App\Repository;

use App\Entity\HeroItems;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method HeroItems|null find($id, $lockMode = null, $lockVersion = null)
 * @method HeroItems|null findOneBy(array $criteria, array $orderBy = null)
 * @method HeroItems[]    findAll()
 * @method HeroItems[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HeroItemsRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, HeroItems::class);
    }

//    /**
//     * @return HeroItems[] Returns an array of HeroItems objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?HeroItems
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function doesItExist($hero_id,$id_decoder){

        // return $this->createQueryBuilder('h')
        // ->andWhere('h.hero = :val')
        // ->setParameter('val', $hero_id)
        // ->andWhere('h.id_decoder = :val')
        // ->setParameter('val', $id_decoder)
        // ->getQuery()
        // ->getOneOrNullResult();

        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT * FROM hero_items
            WHERE id_decoder = :value
            AND hero_id = :value2
        ';

        $stmt = $conn->prepare($sql);
        $stmt->execute(['value' => $id_decoder, 'value2' => $hero_id]);

        return $stmt->fetch();
    }

    public function findItemsByHeroId($value)
    {
        $conn = $this->getEntityManager()->getConnection();

        $sql = '
            SELECT items.* FROM hero_items
            INNER JOIN items
            ON hero_items.item_id = items.id
            WHERE hero_items.hero_id = :value
            ';
        $stmt = $conn->prepare($sql);
        $stmt->execute(['value' => $value]);

        return $stmt->fetchAll();
    }
}
