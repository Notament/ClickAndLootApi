<?php

namespace App\Repository;

use App\Entity\RightHandedWeapon;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method RightHandedWeapon|null find($id, $lockMode = null, $lockVersion = null)
 * @method RightHandedWeapon|null findOneBy(array $criteria, array $orderBy = null)
 * @method RightHandedWeapon[]    findAll()
 * @method RightHandedWeapon[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RightHandedWeaponRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, RightHandedWeapon::class);
    }

//    /**
//     * @return RightHandedWeapon[] Returns an array of RightHandedWeapon objects
//     */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('r.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?RightHandedWeapon
    {
        return $this->createQueryBuilder('r')
            ->andWhere('r.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
