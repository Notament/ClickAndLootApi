<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CellRepository")
 */
class Cell
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Map", inversedBy="cells")
     * @ORM\JoinColumn(nullable=false)
     */
    private $map;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $X_coord;

    /**
     * @ORM\Column(type="integer")
     */
    private $Y_coord;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isDiscovered = false;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $state;

    public function getId()
    {
        return $this->id;
    }

    public function getMap(): ?Map
    {
        return $this->map;
    }

    public function setMap(?Map $map): self
    {
        $this->map = $map;

        return $this;
    }

    public function getXCoord(): ?int
    {
        return $this->X_coord;
    }

    public function setXCoord(?int $X_coord): self
    {
        $this->X_coord = $X_coord;

        return $this;
    }

    public function getYCoord(): ?int
    {
        return $this->Y_coord;
    }

    public function setYCoord(int $Y_coord): self
    {
        $this->Y_coord = $Y_coord;

        return $this;
    }

    public function getIsDiscovered(): ?bool
    {
        return $this->isDiscovered;
    }

    public function setIsDiscovered(bool $isDiscovered): self
    {
        $this->isDiscovered = $isDiscovered;

        return $this;
    }

    public function getState(): ?string
    {
        return $this->state;
    }

    public function setState(string $state): self
    {
        $this->state = $state;

        return $this;
    }
}
