<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ItemsRepository")
 */
class Items
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_decoder;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $complete_item;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $suffixe;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rarity_name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $rarity_color;

    /**
     * @ORM\Column(type="integer")
     */
    private $STR;

    /**
     * @ORM\Column(type="integer")
     */
    private $STA;

    /**
     * @ORM\Column(type="integer")
     */
    private $AGI;

    /**
     * @ORM\Column(type="integer")
     */
    private $WIS;

    /**
     * @ORM\Column(type="integer")
     */
    private $INTEL;

    /**
     * @ORM\Column(type="integer")
     */
    private $LUC;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HeroItems", mappedBy="item", orphanRemoval=true)
     */
    private $heroItems;

    public function __construct()
    {
        $this->heroItems = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getIdDecoder(): ?int
    {
        return $this->id_decoder;
    }

    public function setIdDecoder(int $id_decoder): self
    {
        $this->id_decoder = $id_decoder;

        return $this;
    }

    public function getCompleteItem(): ?string
    {
        return $this->complete_item;
    }

    public function setCompleteItem(string $complete_item): self
    {
        $this->complete_item = $complete_item;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSuffixe(): ?string
    {
        return $this->suffixe;
    }

    public function setSuffixe(string $suffixe): self
    {
        $this->suffixe = $suffixe;

        return $this;
    }

    public function getRarityName(): ?string
    {
        return $this->rarity_name;
    }

    public function setRarityName(string $rarity_name): self
    {
        $this->rarity_name = $rarity_name;

        return $this;
    }

    public function getRarityColor(): ?string
    {
        return $this->rarity_color;
    }

    public function setRarityColor(string $rarity_color): self
    {
        $this->rarity_color = $rarity_color;

        return $this;
    }

    public function getSTR(): ?int
    {
        return $this->STR;
    }

    public function setSTR(int $STR): self
    {
        $this->STR = $STR;

        return $this;
    }

    public function getSTA(): ?int
    {
        return $this->STA;
    }

    public function setSTA(int $STA): self
    {
        $this->STA = $STA;

        return $this;
    }

    public function getAGI(): ?int
    {
        return $this->AGI;
    }

    public function setAGI(int $AGI): self
    {
        $this->AGI = $AGI;

        return $this;
    }

    public function getWIS(): ?int
    {
        return $this->WIS;
    }

    public function setWIS(int $WIS): self
    {
        $this->WIS = $WIS;

        return $this;
    }

    public function getINTEL(): ?int
    {
        return $this->INTEL;
    }

    public function setINTEL(int $INTEL): self
    {
        $this->INTEL = $INTEL;

        return $this;
    }

    public function getLUC(): ?int
    {
        return $this->LUC;
    }

    public function setLUC(int $LUC): self
    {
        $this->LUC = $LUC;

        return $this;
    }

    /**
     * @return Collection|HeroItems[]
     */
    public function getHeroItems(): Collection
    {
        return $this->heroItems;
    }

    public function addHeroItem(HeroItems $heroItem): self
    {
        if (!$this->heroItems->contains($heroItem)) {
            $this->heroItems[] = $heroItem;
            $heroItem->setItem($this);
        }

        return $this;
    }

    public function removeHeroItem(HeroItems $heroItem): self
    {
        if ($this->heroItems->contains($heroItem)) {
            $this->heroItems->removeElement($heroItem);
            // set the owning side to null (unless already changed)
            if ($heroItem->getItem() === $this) {
                $heroItem->setItem(null);
            }
        }

        return $this;
    }
}
