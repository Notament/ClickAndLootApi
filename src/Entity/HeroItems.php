<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HeroItemsRepository")
 */
class HeroItems
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Hero", inversedBy="heroItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $hero;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Items", inversedBy="heroItems")
     * @ORM\JoinColumn(nullable=false)
     */
    private $item;

    /**
     * @ORM\Column(type="integer")
     */
    private $id_decoder;

    public function getId()
    {
        return $this->id;
    }

    public function getHero(): ?Hero
    {
        return $this->hero;
    }

    public function setHero(?Hero $hero): self
    {
        $this->hero = $hero;

        return $this;
    }

    public function getItem(): ?Items
    {
        return $this->item;
    }

    public function setItem(?Items $item): self
    {
        $this->item = $item;

        return $this;
    }

    public function getIdDecoder(): ?int
    {
        return $this->id_decoder;
    }

    public function setIdDecoder(int $id_decoder): self
    {
        $this->id_decoder = $id_decoder;

        return $this;
    }
}
