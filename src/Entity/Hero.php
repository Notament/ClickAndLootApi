<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\HeroRepository")
 */
class Hero
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\User", inversedBy="heroes")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pseudo;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $hero_class;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Map", mappedBy="hero", orphanRemoval=true)
     */
    private $maps;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\HeroItems", mappedBy="hero", orphanRemoval=true)
     */
    private $heroItems;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $current_X;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $current_Y;

    /**
     * @ORM\Column(type="integer")
     */
    private $gold = 100;

    /**
     * @ORM\Column(type="integer")
     */
    private $health = 100;

    /**
     * @ORM\Column(type="integer")
     */
    private $level = 1;

    /**
     * @ORM\Column(type="integer")
     */
    private $total_xp = 50;

    /**
     * @ORM\Column(type="integer")
     */
    private $current_xp = 0;

    /**
     * @ORM\Column(type="integer")
     */
    private $ressource = 100;

    public function __construct()
    {
        $this->maps = new ArrayCollection();
        $this->heroItems = new ArrayCollection();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getPseudo(): ?string
    {
        return $this->pseudo;
    }

    public function setPseudo(string $pseudo): self
    {
        $this->pseudo = $pseudo;

        return $this;
    }

    public function getHeroClass(): ?string
    {
        return $this->hero_class;
    }

    public function setHeroClass(string $hero_class): self
    {
        $this->hero_class = $hero_class;

        return $this;
    }

    /**
     * @return Collection|Map[]
     */
    public function getMaps(): Collection
    {
        return $this->maps;
    }

    public function addMap(Map $map): self
    {
        if (!$this->maps->contains($map)) {
            $this->maps[] = $map;
            $map->setHero($this);
        }

        return $this;
    }

    public function removeMap(Map $map): self
    {
        if ($this->maps->contains($map)) {
            $this->maps->removeElement($map);
            // set the owning side to null (unless already changed)
            if ($map->getHero() === $this) {
                $map->setHero(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|HeroItems[]
     */
    public function getHeroItems(): Collection
    {
        return $this->heroItems;
    }

    public function addHeroItem(HeroItems $heroItem): self
    {
        if (!$this->heroItems->contains($heroItem)) {
            $this->heroItems[] = $heroItem;
            $heroItem->setHero($this);
        }

        return $this;
    }

    public function removeHeroItem(HeroItems $heroItem): self
    {
        if ($this->heroItems->contains($heroItem)) {
            $this->heroItems->removeElement($heroItem);
            // set the owning side to null (unless already changed)
            if ($heroItem->getHero() === $this) {
                $heroItem->setHero(null);
            }
        }

        return $this;
    }

    public function getCurrentX(): ?int
    {
        return $this->current_X;
    }

    public function setCurrentX(?int $current_X): self
    {
        $this->current_X = $current_X;

        return $this;
    }

    public function getCurrentY(): ?int
    {
        return $this->current_Y;
    }

    public function setCurrentY(?int $current_Y): self
    {
        $this->current_Y = $current_Y;

        return $this;
    }

    public function getGold(): ?int
    {
        return $this->gold;
    }

    public function setGold(int $gold): self
    {
        $this->gold = $gold;

        return $this;
    }

    public function getHealth(): ?int
    {
        return $this->health;
    }

    public function setHealth(int $health): self
    {
        $this->health = $health;

        return $this;
    }

    public function getLevel(): ?int
    {
        return $this->level;
    }

    public function setLevel(int $level): self
    {
        $this->level = $level;

        return $this;
    }

    public function getTotalXp(): ?int
    {
        return $this->total_xp;
    }

    public function setTotalXp(int $total_xp): self
    {
        $this->total_xp = $total_xp;

        return $this;
    }

    public function getCurrentXp(): ?int
    {
        return $this->current_xp;
    }

    public function setCurrentXp(int $current_xp): self
    {
        $this->current_xp = $current_xp;

        return $this;
    }

    public function getRessource(): ?int
    {
        return $this->ressource;
    }

    public function setRessource(int $ressource): self
    {
        $this->ressource = $ressource;

        return $this;
    }
}
