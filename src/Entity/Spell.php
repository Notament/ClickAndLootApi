<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SpellRepository")
 */
class Spell
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $class;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $damage;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $per_level;

    /**
     * @ORM\Column(type="decimal", precision=5, scale=2)
     */
    private $stat_dependancy;

    /**
     * @ORM\Column(type="integer")
     */
    private $cost;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $stat_dependancy_name;

    public function getId()
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getClass(): ?string
    {
        return $this->class;
    }

    public function setClass(string $class): self
    {
        $this->class = $class;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDamage()
    {
        return $this->damage;
    }

    public function setDamage($damage): self
    {
        $this->damage = $damage;

        return $this;
    }

    public function getPerLevel()
    {
        return $this->per_level;
    }

    public function setPerLevel($per_level): self
    {
        $this->per_level = $per_level;

        return $this;
    }

    public function getStatDependancy()
    {
        return $this->stat_dependancy;
    }

    public function setStatDependancy($stat_dependancy): self
    {
        $this->stat_dependancy = $stat_dependancy;

        return $this;
    }

    public function getCost(): ?int
    {
        return $this->cost;
    }

    public function setCost(int $cost): self
    {
        $this->cost = $cost;

        return $this;
    }

    public function getStatDependancyName(): ?string
    {
        return $this->stat_dependancy_name;
    }

    public function setStatDependancyName(string $stat_dependancy_name): self
    {
        $this->stat_dependancy_name = $stat_dependancy_name;

        return $this;
    }
}
