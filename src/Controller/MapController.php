<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\Map;
use App\Entity\Hero;

/**
 * @Route("/map", name="map")
 */
class MapController extends Controller
{

    /**
     * @Route("/new", name="newMap", methods="POST")
    */
    public function newMap(Request $request)
    {
        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        $hero = $em->getRepository(Hero::class)->find($data['hero_id']);

        $map = new Map();

        $map->setHero($hero)
            ->setName($data['name'])
            ->setBackground($data['background']);

        $em->persist($map);
        $em->flush();

        return new JsonResponse(['message' => "Created"]);
    }

    /**
     * @Route("/done/{id}", name="finishingMap", methods="PATCH")
     */
    public function finishingMap($id){

        $em = $this->getDoctrine()->getManager();

        $map = $em->getRepository(Map::class)->find($id);

        $map->setIsDone(true);

        $em->persist($map);
        $em->flush();

        return new JsonResponse(['message' => "Map finished wp"]);
    }

    /**
     * @Route("/all/{hero_id}", name="allMapsByHero", methods="GET")
     */
    public function getAllMaps($hero_id){

        $response = [];
        $em = $this->getDoctrine()->getManager();

        $maps = $em->getRepository(Map::class)->findByIdHero($hero_id);

        if(empty($maps)){
            return new JsonResponse(['message' => "No map avaible"]);
        }
        else {
            foreach($maps as $map){

                if(!$map->getIsDone()){
                    $response = $response + ['mapToDo' =>
                    [
                        'id' => $map->getId(),
                        'name' => $map->getName(),
                        'background' => $map->getBackground(),
                        'isDone' => $map->getIsDone()
                    ]
                ];
                }
                else{
                    $response = $response + ['map'. $map->getId().''=>'map '. $map->getName() .' already completed'];
                }

            }

            return new JsonResponse($response);
        }
    }
}
