<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use App\Entity\Hero;
use \App\Entity\User;

/**
 * @Route("/hero", name="hero")
 */
class HeroController extends Controller
{

    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/all/{id_user}", name="getAllHeroesByIdPlayer", methods="GET" )
     */
    public function getAllHeroes($id_user){
        $em = $this->getDoctrine()->getManager();
        $heroes = $em->getRepository(Hero::class)
            ->findByIdUser($id_user);


        $heroes = $this->serializer->serialize($heroes, 'json');

        return new Response($heroes);
    }

    /**
     * @Route("/delete/{id}", name="deleteHero", methods="DELETE" )
     */
    public function deleteHero($id){
        $em = $this->getDoctrine()->getManager();
        $hero = $em->getRepository(Hero::class)
            ->find($id);

        $em->remove($hero);
        $em->flush();

        return new JsonResponse(['message' => "Héros bien delete"]);
    }

    /**
     * @Route("/new", name="newHero", methods="POST")
     */
    public function newHero(Request $request) {

        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        if($data['hero_class'] == 'Archer' || $data['hero_class'] == 'Mage'){

            $hero = new Hero();

            $user = $em->getRepository(User::class)
                    ->find($data['user_id']);

            $hero->setPseudo($data['pseudo'])
                 ->setHeroClass($data['hero_class'])
                 ->setUser($user);

            $em->persist($hero);
            $em->flush();


            return new JsonResponse(['message' => "Personnage crée"]);
        }
        else{
            return new JsonResponse(['message' => "Classe non existante"]);
        }

    }

    /**
     * @Route("/set/position/{id}", name="setCurrentPosition", methods="PATCH")
     */
    public function setCurrentPosition($id, Request $request){

        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        $hero = $em->getRepository(Hero::class)->find($id);

        $hero->setCurrentX($data['X'])
             ->setCurrentY($data['Y']);

        $em->persist($hero);
        $em->flush();

        return new JsonResponse(['message' => 'Done']);
    }

    /**
     * @Route("/set/gold/{id}", name="setGoldHero", methods="PATCH")
     */
    public function setGold($id, Request $request){

        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        $hero = $em->getRepository(Hero::class)->find($id);

        $currentGold = $hero->getGold() + intval($data['sign'].$data['value']);

        $hero->setGold($currentGold);   

        $em->persist($hero);
        $em->flush();

        return new JsonResponse(['message' => 'Done']);
    }

    /**
     * @Route("/set/health/{id}", name="setHealth", methods="PATCH")
     */
    public function setHealth($id, Request $request){

        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        $hero = $em->getRepository(Hero::class)->find($id);

        $currentHealth = $hero->getHealth() + intval($data['sign'].$data['value']);

        if($currentHealth > 100){
            $hero->setRessource(100);   
        }
        else{
            $hero->setRessource($currentHealth);   
        }


        $em->persist($hero);
        $em->flush();

        return new JsonResponse(['message' => 'Done']);
    }
    /**
     * @Route("/set/ressource/{id}", name="setRessource", methods="PATCH")
     */
    public function setRessource($id, Request $request){

        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        $hero = $em->getRepository(Hero::class)->find($id);

        $currentRessource = $hero->getRessource() + intval($data['sign'].$data['value']);

        if($currentRessource > 100){
            $hero->setRessource(100);   
        }
        else{
            $hero->setRessource($currentRessource);   
        }

        $em->persist($hero);
        $em->flush();

        return new JsonResponse(['message' => 'Done']);
    }


    /**
     * @Route("/set/experience/{id}", name="setExperience", methods="PATCH")
     */
    public function setExperience($id, Request $request){

        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        $hero = $em->getRepository(Hero::class)->find($id);

        if(($hero->getCurrentXp() + $data['current_xp']) >= $data['total_xp']  ){

            $currentXp = $data['total_xp'] - ($hero->getCurrentXp() + $data['current_xp']);
            $hero->setCurrentXp(abs($currentXp))
                 ->setTotalXp($data['total_xp'] + 50)
                 ->setLevel($data['level'] + 1);

            $em->persist($hero);
            $em->flush();
            
            return new JsonResponse(['message' => 'Level Up!']);
        }

        $hero->setCurrentXp($hero->getCurrentXp() + $data['current_xp']);  

        $em->persist($hero);
        $em->flush();

        return new JsonResponse(['message' => 'Done']);
    }
}
