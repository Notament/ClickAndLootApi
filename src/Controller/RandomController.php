<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use \App\Entity\ItemType;
use \App\Entity\SuffixeItem;
use \App\Entity\RightHandedWeapon;
use \App\Entity\LeftHandedWeapon;
use \App\Entity\Environnement;
use \App\Entity\SuffixeMap;

/**
 * @Route("/random")
 */
class RandomController extends Controller
{
    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/item_type/{id}", name="itemType",methods="GET")
     */
    public function getItemType($id)
    {
        $em = $this->getDoctrine()->getManager();
        $itemType = $em->getRepository(ItemType::class)
            ->find($id);

        $itemType = $this->serializer->serialize($itemType, 'json');

        return new Response($itemType);
    }

    /**
     * @Route("/suffixe_item/{id}", name="suffixeItem",methods="GET")
     */
    public function getSuffixeItem($id)
    {
        $em = $this->getDoctrine()->getManager();
        $suffixeItem = $em->getRepository(SuffixeItem::class)
            ->find($id);

        $suffixeItem = $this->serializer->serialize($suffixeItem, 'json');

        return new Response($suffixeItem);
    }

    /**
     * @Route("/right_weapon/{id}", name="rightWeapon",methods="GET")
     */
    public function getRightWeapon($id)
    {
        $em = $this->getDoctrine()->getManager();
        $rightWeapon = $em->getRepository(RightHandedWeapon::class)
            ->find($id);

        $rightWeapon = $this->serializer->serialize($rightWeapon, 'json');

        return new Response($rightWeapon);
    }

    /**
     * @Route("/left_weapon/{id}", name="leftWeapon", methods="GET")
     */
    public function getLeftWeapon($id)
    {
        $em = $this->getDoctrine()->getManager();
        $leftWeapon = $em->getRepository(LeftHandedWeapon::class)
            ->find($id);

        $leftWeapon = $this->serializer->serialize($leftWeapon, 'json');

        return new Response($leftWeapon);
    }

    /**
     * @Route("/environnement/{id}", name="environnement",methods="GET")
     */
    public function getEnvironnement($id)
    {
        $em = $this->getDoctrine()->getManager();
        $environnement = $em->getRepository(Environnement::class)
            ->find($id);

        $environnement = $this->serializer->serialize($environnement, 'json');

        return new Response($environnement);
    }

    /**
     * @Route("/suffixe_map/{id}", name="suffixeMap", methods="GET")
     */
    public function getSuffixeMap($id)
    {
        $em = $this->getDoctrine()->getManager();
        $suffixeMap = $em->getRepository(SuffixeMap::class)
            ->find($id);

        $suffixeMap = $this->serializer->serialize($suffixeMap, 'json');

        return new Response($suffixeMap);
    }

        /**
     * @Route("/suffixe_map/{id_sf}/environnement/{id_e}", name="suffixeMap&Environnement", methods="GET")
     */
    public function getSuffixeMapAndEnvironnement($id_sf,$id_e)
    {
        $em = $this->getDoctrine()->getManager();
        $suffixeMap = $em->getRepository(SuffixeMap::class)
            ->find($id_sf);

        $em = $this->getDoctrine()->getManager();
        $environnement = $em->getRepository(Environnement::class)
            ->find($id_e);

        $fullContent = $this->serializer->serialize([$environnement,$suffixeMap], 'json');

        return new Response($fullContent);
    }
}
