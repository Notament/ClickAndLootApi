<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use \App\Entity\Spell;

/**
 * @Route("/spell")
 */
class SpellController extends Controller 
{
    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/get/{class}", name="getSpellsByClass",methods="GET")
     */
    public function getSpellsByClass($class)
    {
        $em = $this->getDoctrine()->getManager();
        $spells= $em->getRepository(Spell::class)
            ->findByClass($class);
        
        if(count($spells) == 0){
            return new JsonResponse(['message' => 'Bien tented']);
        }

        $suffixeItem = $this->serializer->serialize($spells, 'json');

        return new Response($suffixeItem);
    }


}