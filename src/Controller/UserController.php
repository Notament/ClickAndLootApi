<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use App\Entity\User;

use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


/**
 * @Route("/user", name="user")
 */
class UserController extends Controller
{
    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
     * @Route("/new", name="newUser", methods="POST")
     */
    public function register(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        $user_exists = $em->getRepository(User::class)
                        ->findByPseudo($data['pseudo']);

        if(!empty($user_exists)){
            return new JsonResponse(['message' => 'Le pseudo que vous avez écrit est déja pris']);
        }
        else{
            $user = new User();

            $encoded = $encoder->encodePassword($user, $data['password']);

            $user->setPseudo($data['pseudo'])
                 ->setPassword($encoded);


            $em->persist($user);
            $em->flush();

            return new JsonResponse(['message' => 'Utilisateur bien enregistré, vous pouvez maintenant vous connectez avec']);
        }
    }

    /**
     * @Route("/login", name="loginUser" ,methods="POST")
     */
    public function login(Request $request, UserPasswordEncoderInterface $encoder)
    {
        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        $user = $em->getRepository(User::class)
                    ->findByPseudo($data['pseudo']);

        //$user = $user[0];
        if(empty($user)){
            return new JsonResponse(['message' => "Le pseudo que vous avez écrit n'est pas valide"]);
        }
        else{
            $user = $user[0];
            if($encoder->isPasswordValid($user, $data['password'])){
                return new JsonResponse(['id' => $user->getId(), 'pseudo' => $user->getPseudo()]);
            }

            else{
                return new JsonResponse(['message' => "Votre mot de passe est invalide"]);
            }
        }
    }
}
