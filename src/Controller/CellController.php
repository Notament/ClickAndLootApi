<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\Cell;
use App\Entity\Map;


/**
 * @Route("/cell", name="cell")
 */
class CellController extends Controller
{

    public function __construct() {
        $encoder = new JsonEncoder();
        $normalizer = new ObjectNormalizer();

        $normalizer->setCircularReferenceHandler(function ($object) {
            return $object->getId();
        });

        $this->serializer = new Serializer(array($normalizer), array($encoder));
    }

    /**
    * @Route("/new", name="newCell", methods="POST")
    */
    public function createNewCell(Request $request)
    {
        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        $map = $em->getRepository(Map::class)->find($data['map_id']);

        $cell = new Cell();

        $cell->setMap($map)
             ->setXCoord($data['X'])
             ->setYCoord($data['Y'])
             ->setState($data['state']);

        $em->persist($cell);
        $em->flush();

        return new JsonResponse(['message' => 'Done']);
    }

    /**
    * @Route("/all/{id_map}", name="cellsByMap", methods="GET")
    */
    public function getCellsByMap($id_map){

        $em = $this->getDoctrine()->getManager();

        $cells = $em->getRepository(Cell::class)->findByIdMap($id_map);

        $cells = $this->serializer->serialize($cells, 'json');

        return new Response($cells);
    }

    /**
     * @Route("/done/{id}", name="doneCell", methods="GET")
     */
    public function doneCell($id){

        $em = $this->getDoctrine()->getManager();

        $cell = $em->getRepository(Cell::class)->find($id);

        $cell->setIsDiscovered(true);

        $em->persist($cell);
        $em->flush();

        return new JsonResponse(['message' => 'Done']);
    }
}
