<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Component\Serializer\Serializer;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use App\Entity\Hero;
use App\Entity\Items;
use App\Entity\HeroItems;

/**
 * @Route("/item", name="item")
 */
class ItemController extends Controller
{

    /**
     * @Route("/new", name="newItem", methods="POST")
     */
    public function newItem(Request $request)
    {
        $data = $request->getContent();
        $em = $this->getDoctrine()->getManager();

        $data = json_decode($data,true);

        $item = new Items();

        $item->setIdDecoder($data['id_decoder'])
             ->setCompleteItem($data['complete_item'])
             ->setName($data['name'])
             ->setSuffixe($data['suffixe'])
             ->setRarityName($data['rarity_name'])
             ->setRarityColor($data['rarity_color'])
             ->setSTR($data['STR'])
             ->setSTA($data['STA'])
             ->setAGI($data['AGI'])
             ->setWIS($data['WIS'])
             ->setINTEL($data['INT'])
             ->setLUC($data['LUC']);

        $em->persist($item);
        $em->flush();

        $hero = $em->getRepository(Hero::class)->find($data['hero_id']);
        $stuffExist = $em->getRepository(HeroItems::class)->doesItExist($data['hero_id'],$data['id_decoder']);
        $last_item = $em->getRepository(Items::class)->last();
        if(empty($stuffExist)){

            $hero_items = new HeroItems();

            $hero_items->setHero($hero)
                       ->setItem($last_item[0])
                       ->setIdDecoder($data['id_decoder']);

            $em->persist($hero_items);
            $em->flush();

            return new JsonResponse(['message' => 'Created']);
        }
        else {
            $hero_items = $em->getRepository(HeroItems::class)->find($stuffExist['id']);
            $hero_items->setItem($last_item[0]);

            $em->persist($hero_items);
            $em->flush();

            return new JsonResponse(['message' => "Edited"]);
        }

    }

    /**
     * @Route("/all/{id}", name="allItems", methods="GET")
     */
    public function getAllItemByHero($id){

        $em = $this->getDoctrine()->getManager();

        $items = $em->getRepository(HeroItems::class)->findItemsByHeroId($id);

        return new JsonResponse($items);
    }

}
